## 99cloud Java Checkstyle 规范

### 1. 通过 Checkstyle 做代码格式检查

Checkstyle 重点在于代码格式的检查，保持团队内部的编码风格统一。随着版本的不断发展，也会涉及到一些代码分析规则检查，与 PMD、Findbugs、SonarQube 等代码分析工具有稍微重叠，但专业性相差甚远。这里应用 Checkstyle 主要注重于代码格式的检查。

Checkstyle 内置了 [sun_checks](https://github.com/checkstyle/checkstyle/blob/checkstyle-8.45.1/src/main/resources/sun_checks.xml) 和 [google_checks](https://github.com/checkstyle/checkstyle/blob/checkstyle-8.45.1/src/main/resources/google_checks.xml) 检查规则，默认使用 sun_checks 规则。这里制定的规则基于 google_checks 进行修改，基于以下两个思考：

1. 最重要的原因：参看 [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html)，整体比较简短清晰，对采用的规则有说明和示例，可以快速参考。
2. sun_checks 中规则只组装了 Checkstyle 的标准检查，需要去 Checkstyle 检查中查看具体的说明和示例。

Checkstyle 版本和可用规则密切相关。随着版本的不断升级，会带来新的可用规则。在后续应用新规则时将会努力保持向后兼容。

### 2. 制定 99cloud-java-checks.xml 规则

Checkstyle 提供的标准检查可从这里查看：https://checkstyle.org/checks.html

Fork [google_checks](https://github.com/checkstyle/checkstyle/blob/checkstyle-8.45.1/src/main/resources/google_checks.xml) 文件做变更后，得到 99cloud-java-checks.xml 配置文件，见外部文件。相比于 [google_checks](https://github.com/checkstyle/checkstyle/blob/checkstyle-8.45.1/src/main/resources/google_checks.xml)  的变更总结如下。

#### 2.1 google_checks 已有规则变动

1. 修改检查错误级别 `severity` 从 `warning` 到 `error`，即如果检查不过，则默认构建失败。

2. 缩进规则 `Indentation` module，默认基于 2 个空格做调整。修改为基于 4 个空格做调整。

3. 文件行长度规则 `LineLength` module，默认一行最多为 100 个字符。修改为 120 个字符。

4. 命名缩写规则 `AbbreviationAsWordInName` module 变更 `allowedAbbreviationLength` property 从 0 到 2，也就是最多允许 3 个单词的缩写，主要是为了兼容 DO / PO / DTO / BO / VO / UID 这些常用缩写，保持与 alibaba Java 开发手册一致。

    > 这里也可通过 allowedAbbreviations property 来精确指定，这里通过数量的好处是适用性更广，缺点是会引入不可控的缩写。也可同时指定，效果为取并集。

5. 空行规则 `EmptyLineSeparator` module 变更为文件中最多出现一个连续空行。

6. 变量定义距使用距离规则 `VariableDeclarationUsageDistance` module，默认为 3。修改为 99，以忽略这条规则。

7. JavaDoc 注释规则 `SummaryJavadoc` module，默认必须以英文点号（“.”）作为结尾。修改为空，不做结尾符号限定。但在实际编写时，建议如果英文注释则用英文点号（“.”）结尾，中文注释则用中文句号（“。”）结尾。

8. 包名规则 `PackageName` moduel，默认只允许字母开头。修改第二部分开始允许以下划线（“\_”）开头。

#### 2.2 新增以下规则

1. 添加 ‵NewlineAtEndOfFile` module，文件必须以空行结尾。

2. 添加 `TreeWalker`module 的 `IllegalImport` 属性，不能出现非法导入，默认不能导入 sun.* 包下的类。

3. 添加 `TreeWalker`module 的 `RedundantImport` 属性，不能重复导入类。

4. 添加 `TreeWalker`module 的 `UnusedImports` 属性，不能出现未使用的导入类。

5. 添加 ‵JavadocType` module，配置类和接口必须包含 @author 和 @version 标签，alibaba Java 开发手册限定必须包含 @author 标签。参见：[JavadocType](https://checkstyle.sourceforge.io/config_javadoc.html#JavadocType)。

    > checkstyle 的默认规则只可以限定必须包含 author 和 version 标签，不能使用非 Javadoc 标准标签。如果需要支持其他标签，需要定制开发。

#### 2.3 TODO: 编写 99cloud Java Style Guide

规则评审和确定后，可编写类似 [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html) 的在线文档，并加入版本控制。便于团队内部人员参考。

### 3. Maven 中引入 Checkstyle 插件

这里介绍在 Maven 中引入  Checkstyle 插件来集成应用上述规则。其他构建工具如 Gradle 只是工具使用方式不同，检查规则一致。

```xml
<project>
    ... other elements
    <build>
        <pluginManagement>
            <plugins>
                ... other plugins
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-checkstyle-plugin</artifactId>
                    <version>3.1.2</version>
                    <configuration>
                        <configLocation>99cloud-java-checks.xml</configLocation>
                        <consoleOutput>true</consoleOutput>
                        <linkXRef>false</linkXRef>
                        <failsOnError>true</failsOnError>
                    </configuration>
                    <executions>
                        <execution>
                            <id>validate</id>
                            <phase>validate</phase>
                            <goals>
                                <goal>check</goal>
                            </goals>
                        </execution>
                    </executions>
                    <dependencies>
                        <dependency>
                            <groupId>com.puppycrawl.tools</groupId>
                            <artifactId>checkstyle</artifactId>
                            <version>8.45.1</version>
                        </dependency>
                    </dependencies>
                </plugin>
                ... other plugins
            </plugins>
        </pluginManagement>
        <plugins>
            ... other plugins
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
            </plugin>
            ... other plugins
        </plugins>
    </build>
  
    <reporting>
        <plugins>
            ... other plugins
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-checkstyle-plugin</artifactId>
                <configuration>
                    <!-- override default failsOnError value, do not fail the build -->
                    <failsOnError>false</failsOnError>
                </configuration>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>checkstyle</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            ... other plugins
        </plugins>
    </reporting>
    ... other elements
</project>
```

说明：

1. 在 parent POM `pluginManagement` 中引入，方便后续在子模块中和 `reporting` 中统一应用。
2. 当前（2022-03-12），`maven-checkstyle-plugin` 最新插件版本为 3.1.2，依赖的是 Checkstyle 版本为 8.29，按照提示升级为 8 版本中最新的 8.45.1。否则会报版本升级的警告。

   > 因为 Maven 插件依赖的还是 8.x 版本，考虑到兼容性，并未升级到最新的 9 和 10。

3. **检查过程配置在 Maven `validate` 阶段（即生命周期起始阶段），如果 Checkstyle 代码检查未通过，则整个构建过程失败。**
4. `reporting` 中要覆盖 `failsOnError` 设置，才可以正常通过 `mvn site` 生成项目报告。
5. `configLocation` 配置了检查规则的文件位置，支持本地或网络路径。也可不引用外部文件，直接配置在`<configuration>` 的 `<checkstyleRules>` 中。
