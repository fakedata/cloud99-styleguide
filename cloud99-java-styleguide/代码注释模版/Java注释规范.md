### Java注释规范

#### 单行注释

- 全局变量注释：使用“//”对全局变量进行注释说明
- 常量注释：注释说明常量含义。命名清晰明确的常量无需注释
- 方法内注释：对于方法内流程控制，复杂逻辑处理进行注释

#### 源文件/类注释

- 注释样式

```java
/**
* @ClassName UserUserServiceImpl
* @Author admin
* @Description TODO
* @createTime 2022/02/22
*/
public class UserServiceImpl{
  
}
```

#### 方法注释

- 注释样式

```java
/**
* @title generateNikeName
* @description 根据用户名及年龄生成用户昵称
* @author admin
* @param userName
* @param age
* @return java.lang.String
* @throws IOException
* @createTime 2022/02/22
* @updateTime 2022/02/25
*/
public String generateNikeName(String userName, Int age) throws IOException {
  //...
}
```

#### 实体类字段注释

- 注释样式

```java
public class UserDto{
  //用户名称
  private String name;
  
  //是否被删除 0-未删除，1-已删除
  private Boolean isDeleted;
  
  //用户等级。0-青铜等级，1-白银等级，2-黄金等级
  private LevelEnum level;
}
```

- 注释说明
  - PO等类中，在属性名上方一行，使用单行注释说明字段含义
  - 对于布尔类型的数据，须说明不同值对应的含义
  - 对于枚举类型的数据，须说明不同的枚举类型对应的含义