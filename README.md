# cloud99-styleguide

cloud99-styleguide是一个用于维护代码风格的GIT项目

#### 维护范围：
* html/css/js/ts
* java

#### 已有内容：
* 代码注释模版
* 代码风格规范


###  一、代码注释模版
已有前端以及后端代码注释模版详情，以及针对IDEA和WEBSTROM的代码注释模版配置，可用于一键导入。

#### a. 前端
[前端代码注释模版内容](cloud99-html-css-js-ts-styleguid/代码注释模版/前端注释规范.md)  

webstrom导入配置：  
1. 下载[webstrom配置目录](cloud99-html-css-js-ts-styleguid/代码注释模版/webstrom配置)
2. cd webstrom配置/       
3. zip -r ../settings.zip *   
4. 打开WebStorm->File->Manage IDE Settings->Import Settings...    


#### b. Java后端
[后端代码注释模版内容](cloud99-java-styleguide/代码注释模版/Java注释规范.md)  

idea导入配置：  
1. 下载[idea配置目录](cloud99-java-styleguide/代码注释模版/idea配置)  
2. cd idea配置/   
3. zip -r ./settings.zip *  
4. 打开IDEA->File->Manage IDE Settings->Import Settings...  


### 二、代码风格

#### a. 前端
[vue2代码规范与使用方法](cloud99-html-css-js-ts-styleguid/eslint/eslint-config-99cloud-vue2)  
[vue3代码规范与使用方法](cloud99-html-css-js-ts-styleguid/eslint/eslint-config-99cloud-vue3)  

#### b. 后端
[java代码规范与使用方法](cloud99-java-styleguide/代码规范/99cloud-Checkstyle.md)  

### 三、标准文件
#### gitattributes
[gitattributes模版](cloud99-java-styleguide/标准文件/gitattributes)  
使用方式：
#####  为新的Git库设置统一的.gitattributes文件  
1. 在仓库的根目录下创建名为 .gitattributes 的文件  
2. 拷贝模版文件内容  
3. 添加任意想要的规则  

##### 为已有Git库设置统一的.gitattributes文件（重置 GitAttributes）
1. 在上一步的基础上（确保仓库根目录下已经存在.gitattributes文件）  
2. 执行命令git rm --cached -r  
3. 执行命令git reset --hard  

##### 为所有Git库设置统一的.gitattributes文件
1. 执行命令git config --get core.attributesFile  
2. 执行命令git config --global --get core.attributesFile  

#### gitignore
[gitignore模版](cloud99-java-styleguide/标准文件/gitignore)  
使用方式:  
新建项目时拷贝模版文件重命名为.gitignore  
