# eslint-config-99cloud-vue3

## 使用方法

***1.安装***

```shell
npm install -D eslint-config-99cloud-vue3
```

***2.在您的eslint配置中，继承该配置文件***

``` javascript
// .eslinttrc.js
module.exports = {
  root: true,
  // ...
  extends: ["99cloud-vue3"],
  // ...
};
```

## 细节

``` javascript
/**
 * 概览：
 * -------------------------------
 *
 * 本eslint配置继承的规则集：
 *   --  1、eslint:recommended：eslint的官方推荐规则
 *   --  2、plugin:vue/vue3-recommended：vue3的eslint推荐规则集
 *   --  3、plugin:import/recommended：ES6+ import/export语法分析规则集合
 *   --  4、eslint-config-prettier,：关闭所有与prettier冲突的规则
 *
 * 本eslint配置使用的插件：
 *   -- 1、@typescript-eslint/eslint-plugin：为了可以使用typescript相关lilnt规则
 *   -- 2、eslint-plugin-prettier：运行prettier作为eslint的规则
 *   -- 3、eslint-plugin-vue：eslint的vue插件
 *   -- 4、eslint-plugin-import：让eslint可以分析es6+的import/export语法
 *
 * 本eslint配置引入解析器：
 *   -- 1、vue-eslint-parser：.vue文件的的eslint分析器
 *   -- 2、@typescript-eslint/parser：允许eslint分析typescript
 *
 * -------------------------------
 * 具体配置细节详见下方具体代码及注释：
 */

module.exports = {
  env: {
    /**
     * 让eslint可以识别nodejs的全局变量。
     */
    node: true,

    /**
     * vue/setup-compiler-macros是eslint-plugin-vue的配置，
     * 是为了让selint识别<script setup>中的宏命令，
     * 如defineProps和defineEmits，让其不报告no-undef警告。
     * 详情：
     * https://eslint.vuejs.org/user-guide/#faq
     */
    "vue/setup-compiler-macros": true,
  },

  /**
   * vue-eslint-parser是.vue文件的的eslint分析器。
   * 详情：
   * https://eslint.vuejs.org
   */
  parser: "vue-eslint-parser",

  /**
   * 下面的配置，是为了让@typescript-eslint/parser
   * 分析.vue文件中<script>标签内的脚本。
   * parserOptions.parser是vue-eslint-parser
   * 的配置项。
   * 详情：
   * https://eslint.vuejs.org/user-guide/#usage
   */
  parserOptions: {
    /**
     * @typescript-eslint/parser是typescript的selint分析器，
     * 它允许eslint分析typescript。
     * 详情：
     * https://typescript-eslint.io/docs/linting/#details
     */
    parser: "@typescript-eslint/parser",
  },

  /**
   * 下方第一个配置 @typescript-eslint，
   * 是引入了插件 @typescript-eslint/eslint-plugin，
   * 该插件是为了让你可以使用相关规则，即：
   * 下面针对.ts和.tsx的extends中的
   * plugin:@typescript-eslint/recommended。
   * 详情：
   * https://typescript-eslint.io/docs/linting/#details
   *
   * 下方第二个配置 prettier，
   * 是引入了插件eslint-plugin-prettier，
   * 该插件是运行prettier作为eslint的规则。
   * 详情：
   * https://github.com/prettier/eslint-plugin-prettier
   * prettier，一个代码风格规范，流行于全世界。
   * npmjs官方周安装量2860万。
   * 详情：
   * https://prettier.io
   */
  plugins: ["@typescript-eslint", "prettier"],

  extends: [
    /**
     * eslint的官方推荐规则
     * 详情：
     * https://eslint.org/docs/rules/
     */
    "eslint:recommended",

    /**
     * 继承eslint-plugin-vue/vue3-recommended，
     * 它是eslint-plugin-vue对于vue3的推荐规则集合。
     * 详情：
     * https://eslint.vuejs.org/user-guide/#usage
     */
    "plugin:vue/vue3-recommended",

    /**
     * eslint-plugin-import/recommended
     * 是插件eslint-plugin-import的推荐规则，
     * eslint-plugin-import是一个用来分析ES6+
     * import/export语法的插件，用来防止拼错文件
     * 路径和导入名称，该插件深受广大coder喜爱，
     * npmjs.com官方源周安装量1500万。
     * 详情:
     * https://www.npmjs.com/package/eslint-plugin-import
     */
    "plugin:import/recommended",

    /**
     * 继承eslint-config-prettier,
     * 当使用prettier格式化代码风格时，继承该规则，
     * 将会关闭所有与prettier冲突的规则。
     * 详情：
     * https://prettier.io/docs/en/install.html
     * https://www.npmjs.com/package/eslint-config-prettier
     */
    "prettier",
  ],
  rules: {
    /**
     * 下方是eslint-plugin-seslint-plugin-sprettier的推荐配置
     */
    "prettier/prettier": "error",

    /**
     * 下面关闭了arrow-body-style、prefer-arrow-callback
     * 这两个规则，是因为这两个规则与eslint-config-prettier
     * 一起使用的时候会存在问题，这是官方文档特别指出需要这样处理的。
     * 详情：
     * https://github.com/prettier/eslint-plugin-prettier#arrow-body-style-and-prefer-arrow-callback-issue
     */
    "arrow-body-style": "off",
    "prefer-arrow-callback": "off",
  },

  settings: {
    /**
     * 为eslint-plugin-import，针对后缀为ts与tsx的文件，
     * 指定分析器@typescript-eslint/parser。
     */
    "import/parsers": {
      "@typescript-eslint/parser": [".ts", ".tsx"],
    },

    /**
     * 下面是一个resolver的配置，详情：
     * https://www.npmjs.com/package/eslint-plugin-import#resolvers
     * 这里的其实使用了eslint-import-resolver-typescript，
     * 它的作用是增加eslint-plugin-import对typescript的支持。
     * 详情：
     * https://github.com/alexgorbatchev/eslint-import-resolver-typescript
     */
    "import/resolver": {
      typescript: {
        /**
         * 这个配置是eslint-plugin-import对typescript的配置，
         * 具体作用是尝试解析<root>@types目录。
         */
        alwaysTryTypes: true,
      },
    },
  },

  /**
   * 下面使用overrides的方式，进一步明确了对于后缀为ts与tsx的文件，
   * 执行plugin:@typescript-eslint/recommended规则。因为
   * typescript是javascript的超集，代表了个别规则，对于js不适用，
   * 所以这里做了特殊处理。而再次引入eslint-config-pretiier（即
   * 下面extends中的prettier）,是因为overrides中的配置优先级较高，这样
   * plugin:@typescript-eslint/recommended中如果现在或者将来存在
   * 与prettier冲突的规则，则又会把这些规则再次打开，所以再次引入
   * eslint-config-pretiier关闭所有与prettier冲突的规则。
   */
  overrides: [
    {
      files: ["*.ts", "*.tsx"],
      /**
       * plugin:@typescript-eslint/recommended
       * 是typescript-eslint的推荐规则集。
       */
      extends: ["plugin:@typescript-eslint/recommended", "prettier"],
    },
  ],
};
```
